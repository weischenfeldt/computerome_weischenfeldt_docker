![build_status](https://img.shields.io/docker/build/weischenfeldt/computerome_weischenfeldt.svg)
![docker_pulls](https://img.shields.io/docker/pulls/weischenfeldt/computerome_weischenfeldt.svg)
![docker_builds](https://img.shields.io/docker/automated/weischenfeldt/computerome_weischenfeldt.svg)

# Computerome Weischenfeldt lab Docker image

Docker image containing an environment similar to the computerome environment
and containing bioinformatic tools used and developed by the Weischenfeldt lab.