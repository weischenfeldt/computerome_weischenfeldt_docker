#%Module1.0######################################################################
#
# SAMtools 0.x
#
# Author:	Erland Hochheim
# Date:		2015.10.22
#
#################################################################################
# Create symbolic link to version names: ln -s .module 2.1.0


# Help message
proc ModulesHelp { } {
   set name [file dirname [module-info name]]
   set version [file tail [module-info name]]
   puts stderr "\tLoads the ${name} version ${version} environment"
   puts stderr "\n\tFor further information, use 'module display [module-info name]'"
}

# Additional display message
proc ModulesDisplay { } {
   #set name [file dirname [module-info name]]
   #set version [file tail [module-info name]]
   #puts stderr "<Some extra information to be displayed - ex. what the tool does, etc>"
}

# Whatis message
#module-whatis "Loads the [module-info name] environment"
module-whatis "SAMtools [file tail [module-info name]] - various utilities for manipulating alignments in the SAM (Sequence Alignment/Map) format"

# Set variables
set name [file dirname [module-info name]]
set version [file tail [module-info name]]

# Conflicts and prerequisites
conflict ${name}
conflict bcftools
#prereq <some_module>

# Modify environment variables
#set root /services/tools/${name}-${version}
set root /services/tools/${name}/${version}
prepend-path PATH ${root}/bin
prepend-path MANPATH ${root}/share/man
#prepend-path LD_LIBRARY_PATH ${root}/lib
#prepend-path CPATH ${root}/include
#prepend-path PKG_CONFIG_PATH ${root}/lib/pkgconfig
#prepend-path INFOPATH ${root}/share/info
#prepend-path EMACSLOADPATH ${root}/share/emacs/site-lisp
#prepend-path LIBEXEC_DIR ${root}/libexec/${name}-${version}
#prepend-path PERL5LIB ${root}/lib
#prepend-path PYTHONPATH ${root}/lib/python2.7/site-packages
#prepend-path PYTHONUSERBASE ${root}/python-support
setenv SAMTOOLS ${root}
